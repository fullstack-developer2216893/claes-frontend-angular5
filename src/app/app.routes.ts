import { Routes, RouterModule } from '@angular/router';
import { ArticlesComponent } from './articles/articles/articles.component';
import { ArticleDetailComponent } from './articles/article-detail/article-detail.component';

/**
 * Routing
 */
export const appRoutes: Routes = [
  { path: 'article', component: ArticlesComponent },
  { path: 'article-detail', component: ArticleDetailComponent },
];

