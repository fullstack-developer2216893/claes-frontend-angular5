import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest,HttpHeaders,HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import {SlimLoadingBarService} from "ng2-slim-loading-bar";

@Injectable()
export class ApiAuthInterceptor implements HttpInterceptor {
  constructor(private _loadingBar: SlimLoadingBarService) {}
  intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this._loadingBar.start();
    const headers = new HttpHeaders({
      'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijc4NTI3Y2U4NTM4MmVkM2Q5MjEwMzUxNmU1NjlmNDhhZWViYmE3NzQ1NmUyNTI5OGM4OWRmYWZhOGJjMDlhN2M3Yzk4OTUzNGU5OTgxNzRiIn0.eyJhdWQiOiIxIiwianRpIjoiNzg1MjdjZTg1MzgyZWQzZDkyMTAzNTE2ZTU2OWY0OGFlZWJiYTc3NDU2ZTI1Mjk4Yzg5ZGZhZmE4YmMwOWE3YzdjOTg5NTM0ZTk5ODE3NGIiLCJpYXQiOjE1MjE0NTMzOTAsIm5iZiI6MTUyMTQ1MzM5MCwiZXhwIjoxNTUyOTg5MzkwLCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.H9EDubrLsvJTF7g0eWnyV94coDBMPCrKrWENTMwhcFirB7k_-EN3eYbcVU2lFgUaD4ONbOc6sCG92Gft2_mlilNa5BArEz8kREqcN_MSD3hLS8esLPsxx51QGPZFssbIXwRlasFdAngE3-CZ8Q6UdfCW_6BrbA3C38tpkM7xaz4eg19bq8GN2PT0gwEM8fWFpVz299gCjJOhqhXrRUh0tids0vs9wuz3nzmN0AfZB863QqMB3OJcakGaMEWTOKTu7NE9meq_IHhbJA4fq3SINVBp61rzLCe6VYwZUQaUmQ6o5tJBv0q51Y6QPI9LJmSmExyWK1fPD9-FdEslBSrjWnZXcRu_hZnEYbLz52j-fq-TcI72DpiIeKj8QvvWD93qxInp-udD3tDxYa-l5YbTr2ZwlZLIGr2eRsaX1GneIN3n_lcROsKil2yAceCmJEW9PGLvo1glFVlqOc_hR5HJNNVu2qON-nRFWSDF0U0vFG0cX8yldEL3BB0l1K4ePbc3v4frM6M4fVfZOkwe3vieEW8V4a3dzNaJNsh2jNI7Lj7NUitYiZ0SzvKk0UsEtDftXyPm8BI-mEUAZy7uaIlRq4LCJWSkKxIJ8fua1Bd12boFUwG4nrFX5tTlEWA1TaC_o5_-Q0IJeJQi7P4sxIAmmCiJgFJ6a7nVAItoXD9H5G0',
      //'Content-Type': 'application/json'
      //'Content-Type':'application/x-www-form-urlencoded'
    });
    const authReq = req.clone({headers});
      
    
    return next.handle(authReq).do((event: HttpEvent<any>) => {
      // if the event is for http response
      if (event instanceof HttpResponse) {
        // stop our loader here
        this._loadingBar.complete();
      }

    }, (err: any) => {
      // if any error (not for just HttpResponse) we stop our loader bar
      this._loadingBar.complete();
    });
  }
}