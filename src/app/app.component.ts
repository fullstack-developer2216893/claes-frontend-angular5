import { Component, ViewEncapsulation, AfterViewChecked, OnInit, AfterViewInit } from '@angular/core';
import { ApiService } from './api.service';
declare var $:any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None // make styles global
})
export class AppComponent implements OnInit,AfterViewInit {
  title = 'app';
  selectedNavigation:string;

  constructor(private apiservice:ApiService){
    
  }

  ngOnInit(){
    this.apiservice.selectedNavMenu='Artiklar';
    
  }
  ngAfterViewInit(){
    $('.ui.sidebar').sidebar('attach events', '.toc.item');
  }

  
  
}
