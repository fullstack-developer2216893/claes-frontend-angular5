import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { cafeteriaModel } from '../cafeteria.model';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-cafeteria-listing',
  templateUrl: './cafeteria-listing.component.html',
  styleUrls: ['./cafeteria-listing.component.css']
})
export class CafeteriaListingComponent implements OnInit {
  private cafeterias:cafeteriaModel[]=[];
  p: number = 1;

  startDate: Date = new Date();
  endDate: Date = new Date();
    settings = {
        bigBanner: false,
        timePicker: false,
        format: 'yyyy-MM-dd',
        defaultOpen: false
    }
  

  constructor(private apiService:ApiService,private router: Router) { }
  
  ngOnInit(){
    this.getCafeteriaListing('mackerias');
    $('#mycalendar').calendar();
  }

  //get cafeteria listing with API call
  getCafeteriaListing(url){
    this.cafeterias=[];
    this.apiService.apigetData(url)
    .subscribe(res => {
      const cafeteriaLength=Object.keys(res).length;
      if(cafeteriaLength>0) {
        for (let prop in res) {
          res[prop].deliveryStatus=(res[prop].delivered)?'Ja':'Nej';
          this.cafeterias.push(new cafeteriaModel(res[prop].id,res[prop].customer_id,
            res[prop].contact_id,res[prop].delivery_date,res[prop].delivered,res[prop].info,
            res[prop].total,res[prop].vat_total,res[prop].total_incl_vat,res[prop].deleted_at,
            res[prop].created_at,res[prop].updated_at,res[prop].customer_name,res[prop].contact_name,
            res[prop].contact_email,res[prop].contact_moble,res[prop].deliveryStatus));
          }
        }
    });
  }

  //get cafeteria detail with API call
  cafeteriaDetail(url){
    let formData=new FormData();
      this.apiService.apiPostData(url,formData)
     .subscribe(res => {
       const cafeteriaLength=Object.keys(res).length;
       if(cafeteriaLength>0) { 
            this.router.navigateByUrl('cafeteria-detail/'+res['id']);
         }
     });
  }

  //edit cafeteria
  editCafeteria(cafeteria){
    this.router.navigateByUrl('cafeteria-detail/'+cafeteria.id);
  }

  //all records checkbox
  allRecord(ev){
    if(ev.target.checked){
      this.getCafeteriaListing('mackerias?all=1');
    }else{
      this.getCafeteriaListing('mackerias');
    }
  }

  //print whole page
  onPrint(){
    window.print()
  }

  //start date filteration
  onStartDateSelect(ev){
    const fullDate=this.generateFullDate(ev);
    this.startDate=new Date(fullDate);
    this.dateFilterationResult(fullDate);    
  }

  //end date filteration
  onEndDateSelect(ev){
    const fullDate=this.generateFullDate(ev);
    const startDate=this.generateFullDate(this.startDate);
    const endDate=this.endDate;
    
    /*if(this.endDate<this.startDate){
      alert('End date must be greater than Start date');
      this.endDate=new Date(endDate);
    }else{*/
      this.endDate=new Date(fullDate);
      this.dateFilterationResult(startDate,fullDate); 
    //}

       
  }

  //date filteration method
  dateFilterationResult(start,end=''){
    this.getCafeteriaListing('mackerias?all=1&startdate='+start+'&endDate='+end);
  }

  //generate full date
  generateFullDate(dt){
    let month=dt.getMonth()+1;
    month=(month>9)?month:'0'+month;
    const year=dt.getFullYear();
    const date=dt.getDate();
    return year+'-'+month+'-'+date;
  }

}
