export class cafeteriaModel{
    constructor(public id:number,public customerId:number,public contactId:number,
        public deliveryDate:string,public delivered:number,public info:string,public total:number,
        public vatTotal:number,public totalInclVat:number,public deletedAt:string,
        public createdAt:string,public updatedAt:string,public customerName:string,public userName:string,
        public userEmail:string,public userMobile:string,public deliveryStatus:string){}
}