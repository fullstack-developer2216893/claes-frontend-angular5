export class cafeteriaArticleModel{
    constructor(public id:number,public mackeriaId:number,public articleId:number,
        public date:string,public quantity:number,public name:string,public price :number,
        public total:number,public vatTotal:number,public totalInclVat:number,public deletedAt:string,
        public createdAt:string,public updatedAt:string){}
}