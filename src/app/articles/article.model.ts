export class articleModel{
  constructor(public id:number,public name:string,public price:number,public article_group:number,
    public unit:number,public result_unit:number,public vat_level:number,public date:string,
    public layout1:number,public layout2:number,public layout3:number,public bookable:number,
    public info:string,public image:string,public package_id:number=0){}
}