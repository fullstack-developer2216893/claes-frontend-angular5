import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { articleModel } from '../article.model';
import { ApiService } from '../../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})

export class ArticlesComponent implements OnInit {
  articles:articleModel[]=[];
  private resultUnits=[];
  private units=[];
  articlesArr=[];
  p: number = 1;
  constructor(private apiService:ApiService,private router: Router) {}

  ngOnInit() {
    this.getArticleListing('articles');
    this.getSettings('settings/filterByName/result-units','Result Units');
    this.getSettings('settings/filterByName/units','Units');
  }

  //get data of settings table from db
  getSettings(url,type){
    this.apiService.apigetData(url)
    .subscribe(res => {
      const articleLength=Object.keys(res).length;
        if(articleLength>0) {
          switch(type){
            case 'Result Units':
            this.resultUnits=JSON.parse(res.value);
            break;
            case 'Units':
            this.units=JSON.parse(res.value);
            break;
          }
        }
    });
  }

  //get all articles from db
  getArticleListing(url){
      this.apiService.apigetData(url)
    .subscribe(res => {
      const articleLength=Object.keys(res).length;
      if(articleLength>0) {
        for (let prop in res) {
            var groupname=(res[prop].group)?res[prop].group.name:'';
            this.articles.push(new articleModel(res[prop].id,res[prop].name,res[prop].price,
              groupname,res[prop].unit,res[prop].result_unit,res[prop].vat_level,res[prop].date,
              res[prop].layout1,res[prop].layout2,res[prop].layout3,res[prop].bookable,
              res[prop].info,res[prop].image));
        }
        }
    });
  }

  //redirect to article detail page
  editArticle(article:articleModel){
    this.router.navigateByUrl('article-detail/'+article.id);
  }
  
  //header checbox filteration
  filterResultUnit(value,ev){
    if(ev.target.checked){
        this.articlesArr.push(value);
    }else {
      for(var i=0 ; i < this.resultUnits.length; i++) {
        if(this.articlesArr[i] == value){
          this.articlesArr.splice(i,1);
        }
      }
    }
    let filterArr=this.articlesArr;
    this.articles.length=0;
    let formData=new FormData();
    formData.append('field','result_unit');
    formData.append('value',filterArr.toString());

      //call api service
      this.apiService.apiPostData('filterBy',formData)
      .subscribe(res => {
        const articleLength=Object.keys(res).length;
        if(articleLength>0) {
          for (let prop in res) {
              var groupname=(res[prop].group)?res[prop].group.name:'';
              this.articles.push(new articleModel(res[prop].id,res[prop].name,res[prop].price,groupname,res[prop].unit,res[prop].result_unit,res[prop].vat_level,res[prop].date,res[prop].layout1,res[prop].layout2,res[prop].layout3,res[prop].bookable,res[prop].info,res[prop].image));
            }
            
        }
      });
    }

    articleDetail(url){
      let formData=new FormData();
      this.apiService.apiPostData(url,formData)
     .subscribe(res => {
       const articleLength=Object.keys(res).length;
       if(articleLength>0) { 
            this.router.navigateByUrl('article-detail/'+res['id']);
         }
     });
    }

    //print whole page
    onPrint(){
      window.print()
    }
}
