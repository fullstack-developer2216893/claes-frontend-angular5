import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes,RouterLinkActive } from '@angular/router';
import { NgModule } from '@angular/core';
import { NgSemanticModule } from 'ng-semantic';
import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { StyleGuideComponent } from './components/style-guide/style-guide.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ArticlesModule } from './articles/articles.module';
import { CafeteriaModule } from './cafeteria/cafeteria.module';
import { SharedModule } from './shared/shared.module';
import { EventModule } from './event/event.module';
import { ApiAuthInterceptor } from './apiauth.interceptor';
import { ApiService } from './api.service';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { AlertModule } from 'ngx-alerts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


/**
 * Routing
 */
const appRoutes: Routes = [
  { path: 'style-guide', component: StyleGuideComponent },
  { path: '',   redirectTo: '/style-guide', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];



@NgModule({
  declarations: [
    AppComponent,
    StyleGuideComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    NgSemanticModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes, { enableTracing: true }),
    ArticlesModule,
    CafeteriaModule,
    SharedModule,
    EventModule,
    SlimLoadingBarModule.forRoot(),
    AlertModule.forRoot({maxMessages: 5, timeout: 5000}),
    BrowserAnimationsModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: ApiAuthInterceptor,
    multi: true
  },ApiService
],
  bootstrap: [AppComponent]
})
export class AppModule { }
