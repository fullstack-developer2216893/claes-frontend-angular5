import { Timestamp } from "rxjs";

export class ParticipantModel{
    constructor(public id:number,public date:string,public name:string,public company:string,public mobile:string,public email:string,public eventId:number,public createdAt:string,public updatedAt:string){}
  }