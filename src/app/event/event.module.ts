import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSemanticModule } from 'ng-semantic';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { EventListingComponent } from './event-listing/event-listing.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { FormsModule }   from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgxPaginationModule} from 'ngx-pagination';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';

/**
 * Routing
 */
const appRoutes: Routes = [
	{ path: 'events', component: EventListingComponent },
  { path: 'event-detail/:id', component: EventDetailComponent },
];

@NgModule({
  imports: [
    CommonModule,
    NgSemanticModule,
    RouterModule.forRoot(appRoutes, { enableTracing: true }),
    SharedModule,
    FormsModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    AngularDateTimePickerModule,
    BootstrapModalModule.forRoot({container:document.body})
  ],
  declarations: [EventListingComponent, EventDetailComponent],
  exports: [EventListingComponent,EventDetailComponent]
})
export class EventModule { }
