import { Timestamp } from "rxjs";

export class eventModel{
    constructor(public id:number,public date:string,public name:string,public location:string,
      public timeFrom:string,public timeTo:string,public maxPersons:number,public price:number,
      public info:string,public image:string,public createdAt:string,public participants:number=0){}
  }