import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-mobile-header',
  templateUrl: './mobile-header.component.html',
  styleUrls: ['./mobile-header.component.css']
})
export class MobileHeaderComponent implements OnInit {
  menuItems:Array<any> = [
    /*{title: 'Style Guide', path: '/style-guide'},*/
    {title: 'Startsida', path: '#'},
    {title: 'Bokning', path: '#'},
    {title: 'Medlemmar', path: '#'},
    {title: 'Offert', path: '#'},
    {title: 'Artiklar', path: 'article'},
    {title: 'Faktura', path: '#'},
    {title: 'Mackeria', path: 'cafeteria'},
    {title: 'Event', path: 'events'},
    {title: 'Partners/Lev', path: '#'},
    {title: 'Personer', path: '#'},
    {title: 'Inställningar', path: '#'},
    {title: 'Statistik', path: '#'},
    /*{title: '404', path: '/other'}*/
  ];

  constructor(private router: Router) { }

  ngOnInit() {
  }

}
