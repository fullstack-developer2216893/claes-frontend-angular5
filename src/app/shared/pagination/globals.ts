export const GlobalVariable = Object.freeze({
    BASE_API_URL: 'http://localhost/api',
    BASE_CSS_URL: 'http://localhost/api/public/css',
    BASE_ASSET_URL: 'http://localhost/admin/src/assets',
    ITEM_PAGE:10
})
