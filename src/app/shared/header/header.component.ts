import { Component, OnInit, EventEmitter, Output, OnChanges, AfterContentChecked } from '@angular/core';
import { ApiService } from '../../api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  menuItems:Array<any> = [
    /*{title: 'Style Guide', path: '/style-guide'},*/
    {title: 'Startsida', path: '#'},
    {title: 'Bokning', path: '#'},
    {title: 'Medlemmar', path: '#'},
    {title: 'Offert', path: '#'},
    {title: 'Artiklar', path: 'article'},
    {title: 'Faktura', path: '#'},
    {title: 'Mackeria', path: 'cafeteria'},
    {title: 'Event', path: 'events'},
    {title: 'Partners/Lev', path: '#'},
    {title: 'Personer', path: '#'},
    {title: 'Inställningar', path: '#'},
    {title: 'Statistik', path: '#'},
    /*{title: '404', path: '/other'}*/
  ];

  
  @Output() onSelectedNavigation=new EventEmitter<string>();
  constructor(private apiservice:ApiService,private router: Router) {
    
  }

  onSelectNav(menu){
    this.apiservice.selectedNavMenu=menu;
  }

  

}
